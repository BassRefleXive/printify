<?php

declare(strict_types = 1);

namespace App\Component\Location\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Country
{
    private $codeIso3;
    private $codeIso2;
    private $name;
    private $nameOfficial;
    private $latitude;
    private $longitude;
    private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function codeIso3(): string
    {
        return $this->codeIso3;
    }

    public function codeIso2(): string
    {
        return $this->codeIso2;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function nameOfficial(): string
    {
        return $this->nameOfficial;
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }

    public function orders(): array
    {
        return $this->orders->toArray();
    }
}
