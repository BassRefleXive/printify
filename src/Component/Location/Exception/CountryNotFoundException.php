<?php

declare(strict_types=1);

namespace App\Component\Location\Exception;

class CountryNotFoundException extends \RuntimeException implements LocationExceptionInterface
{
    public static function missingIso2Code(string $code): self
    {
        return new self(sprintf('Missing country by ISO 3166-1 alpha-2 code "%s".', $code));
    }
}
