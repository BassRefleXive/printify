<?php

declare(strict_types=1);

namespace App\Component\Location\Exception;

interface LocationExceptionInterface extends \Throwable
{
}
