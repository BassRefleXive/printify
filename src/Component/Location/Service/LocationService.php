<?php

declare(strict_types = 1);

namespace App\Component\Location\Service;

use App\Application\Repository\Location\CountryRepositoryInterface;
use App\Component\Location\Model\Country;

class LocationService
{
    private const DEFAULT_COUNTRY = 'US';

    private $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function defaultCountry(): Country
    {
        return $this->countryRepository->getByIso2Code(self::DEFAULT_COUNTRY);
    }
}
