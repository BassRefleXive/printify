<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway\Response;

class Location
{
    private $countryCode;

    public function __construct(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }

    public function countryCode(): string
    {
        return $this->countryCode;
    }
}
