<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway;

use App\Component\Location\Discoverer\Gateway\Response\Location;

interface GatewayInterface
{
    public function discover(string $ip): Location;
}
