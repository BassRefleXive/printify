<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway\FreeGeoIp\Assembler;

use App\Component\Core\Http\Assembler\RequestAssembler as BaseRequestAssembler;

class RequestAssembler extends BaseRequestAssembler
{
}
