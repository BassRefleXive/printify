<?php

declare(strict_types = 1);

namespace App\Component\Location\Discoverer\Gateway\FreeGeoIp\Criteria;

use App\Component\Core\Http\Criteria\UriCriteria as BaseUriCriteria;
use App\Component\Core\Http\Criteria\UriCriteriaInterface;

final class UriCriteria extends BaseUriCriteria implements UriCriteriaInterface
{
    public static function discover(string $ip, string $key): self
    {
        return new self(
            '{ip}',
            [
                'ip' => $ip,
            ],
            [
                'access_key' => $key,
            ]
        );
    }
}
