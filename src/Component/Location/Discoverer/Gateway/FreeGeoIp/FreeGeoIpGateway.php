<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway\FreeGeoIp;

use App\Component\Core\Http\Assembler\RequestAssembler;
use App\Component\Core\Http\Criteria\RequestCriteria;
use App\Component\Core\Http\HttpClientInterface;
use App\Component\Core\Http\Response\JsonResponseParser;
use App\Component\Location\Discoverer\Gateway\FreeGeoIp\Criteria\UriCriteria;
use App\Component\Location\Discoverer\Gateway\FreeGeoIp\Factory\LocationFactory;
use App\Component\Location\Discoverer\Gateway\GatewayInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Component\Location\Discoverer\Gateway\Response\Location;

class FreeGeoIpGateway implements GatewayInterface
{
    private $client;
    private $requestAssembler;
    private $responseParser;
    private $locationFactory;
    private $apiKey;

    public function __construct(HttpClientInterface $client, RequestAssembler $requestAssembler, string $apiKey)
    {
        $this->client = $client;
        $this->requestAssembler = $requestAssembler;
        $this->apiKey = $apiKey;
        $this->responseParser = new JsonResponseParser();
        $this->locationFactory = new LocationFactory();
    }

    public function discover(string $ip): Location
    {
        $requestCriteria = new RequestCriteria(
            UriCriteria::discover($ip, $this->apiKey),
            Request::METHOD_GET
        );

        $response = $this->client->send($this->requestAssembler->fromCriteria($requestCriteria));

        return $this->locationFactory->fromArray($this->responseParser->responseArray($response));
    }
}
