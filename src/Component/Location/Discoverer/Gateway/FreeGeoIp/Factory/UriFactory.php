<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway\FreeGeoIp\Factory;

use App\Component\Core\Http\Factory\UriFactory as BaseUriFactory;

class UriFactory extends BaseUriFactory
{
}
