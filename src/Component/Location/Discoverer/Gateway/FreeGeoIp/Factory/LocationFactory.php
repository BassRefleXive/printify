<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway\FreeGeoIp\Factory;

use App\Component\Location\Discoverer\Gateway\FreeGeoIp\Exception\ResponseException;
use App\Component\Location\Discoverer\Gateway\Response\Location;

class LocationFactory
{
    public function fromArray(array $data): Location
    {
        if (isset($data['error'])) {
            throw ResponseException::error($data['error']['info'] ?? 'Unknown');
        }

        if (!isset($data['country_code'])) {
            throw ResponseException::missingCountryCode();
        }

        return new Location($data['country_code']);
    }
}
