<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Gateway\FreeGeoIp\Client;

use App\Component\Core\Http\GuzzleHttp\Client as BaseClient;

class Client extends BaseClient
{
}
