<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Exception;

interface DiscovererExceptionInterface extends \Throwable
{
}
