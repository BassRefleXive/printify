<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer\Exception;

use App\Component\Location\Discoverer\Gateway\Exception\GatewayExceptionInterface;
use App\Component\Location\Exception\LocationExceptionInterface;

class DiscovererException extends \RuntimeException implements DiscovererExceptionInterface
{
    public static function gateway(GatewayExceptionInterface $e): self
    {
        return new self(
            'Gateway exception occurred.',
            $e->getCode(),
            $e
        );
    }

    public static function missingCountry(LocationExceptionInterface $e): self
    {
        return new self(
            'Missing country in our database.',
            $e->getCode(),
            $e
        );
    }
}
