<?php

declare(strict_types=1);

namespace App\Component\Location\Discoverer;

use App\Application\Repository\Location\CountryRepositoryInterface;
use App\Component\Location\Discoverer\Exception\DiscovererException;
use App\Component\Location\Discoverer\Gateway\Exception\GatewayExceptionInterface;
use App\Component\Location\Discoverer\Gateway\GatewayInterface;
use App\Component\Location\Exception\LocationExceptionInterface;
use App\Component\Location\Model\Country;

class LocationDiscoverer
{
    private $gateway;
    private $countryRepository;

    public function __construct(GatewayInterface $gateway, CountryRepositoryInterface $countryRepository)
    {
        $this->gateway = $gateway;
        $this->countryRepository = $countryRepository;
    }

    public function country(string $ip): Country
    {
        try {
            $gatewayCountry = $this->gateway->discover($ip);
        } catch (GatewayExceptionInterface $e) {
            throw DiscovererException::gateway($e);
        }

        try {
            $country = $this->countryRepository->getByIso2Code($gatewayCountry->countryCode());
        } catch (LocationExceptionInterface $e) {
            throw DiscovererException::missingCountry($e);
        }

        return $country;
    }
}
