<?php

declare(strict_types = 1);

namespace App\Component\Location\Dto;

class CountryDto
{
    /**
     * @var string
     */
    public $codeIso3;

    /**
     * @var string
     */
    public $codeIso2;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $nameOfficial;
}
