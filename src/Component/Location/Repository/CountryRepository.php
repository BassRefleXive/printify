<?php

declare(strict_types = 1);

namespace App\Component\Location\Repository;

use App\Application\Repository\Location\CountryRepositoryInterface;
use App\Component\Core\Doctrine\Repository\BaseRepository;
use App\Component\Location\Exception\CountryNotFoundException;
use App\Component\Location\Model\Country;
use Doctrine\Common\Persistence\ManagerRegistry;

class CountryRepository extends BaseRepository implements CountryRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function getByIso2Code(string $iso2Code): Country
    {
        /** @var Country|null $country */
        $country = $this->findOneBy([
            'codeIso2' => $iso2Code,
        ]);

        if (null === $country) {
            throw CountryNotFoundException::missingIso2Code(mb_strtolower($iso2Code));
        }

        return $country;
    }
}
