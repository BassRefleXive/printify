<?php

declare(strict_types = 1);

namespace App\Component\Location\Assembler;

use App\Component\Location\Dto\CountryDto;
use App\Component\Location\Model\Country;

class CountryDtoAssembler
{
    public function fromModel(Country $country): CountryDto
    {
        $dto = new CountryDto();

        $dto->codeIso3 = $country->codeIso3();
        $dto->codeIso2 = $country->codeIso2();
        $dto->name = $country->name();
        $dto->nameOfficial = $country->nameOfficial();

        return $dto;
    }
}
