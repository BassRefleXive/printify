<?php

declare(strict_types = 1);

namespace App\Component\Product\Service;

use App\Application\Repository\Product\ProductRepositoryInterface;
use App\Component\Product\Assembler\ProductDtoAssembler;
use App\Component\Product\Command\CreateProductCommand;
use App\Component\Product\Dto\ProductDto;
use App\Component\Product\Model\Product;

class ProductService
{
    private $productRepository;
    private $productDtoAssembler;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->productDtoAssembler = new ProductDtoAssembler();
    }

    public function create(CreateProductCommand $command): ProductDto
    {
        $this->productRepository->checkUniqueness($command->type(), $command->size(), $command->color());

        $product = new Product(
            $this->productRepository->nextIdentity(),
            $command->price(),
            $command->type(),
            $command->size(),
            $command->color()
        );

        $this->productRepository->save($product);

        return $this->productDtoAssembler->fromModel($product);
    }

    public function all(): array
    {
        return array_map([$this->productDtoAssembler, 'fromModel'], $this->productRepository->findAll());
    }
}
