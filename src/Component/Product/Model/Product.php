<?php

declare(strict_types = 1);

namespace App\Component\Product\Model;

use App\Component\Product\Enum\ProductSize;
use App\Component\Product\Enum\ProductType;
use Money\Currency;
use Money\Money;
use Ramsey\Uuid\UuidInterface;

class Product
{
    private $id;
    private $price;
    private $currency;
    private $type;
    private $size;
    private $color;

    public function __construct(UuidInterface $id, Money $price, ProductType $type, ProductSize $size, int $color)
    {
        $this->id = $id;
        $this->type = $type;
        $this->size = $size;
        $this->color = $color;
        $this->price = $price->getAmount();
        $this->currency = $price->getCurrency()->getName();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function type(): ProductType
    {
        return $this->type;
    }

    public function size(): ProductSize
    {
        return $this->size;
    }

    public function color(): int
    {
        return $this->color;
    }

    public function price(): Money
    {
        return new Money($this->price, new Currency($this->currency));
    }
}
