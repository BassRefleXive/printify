<?php

declare(strict_types = 1);


namespace App\Component\Product\Enum;

use MabeEnum\Enum;

final class ProductSize extends Enum
{
    public const S = 's';
    public const M = 'm';
    public const L = 'l';
    public const XL = 'xl';
}
