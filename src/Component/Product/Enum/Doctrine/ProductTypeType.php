<?php

declare(strict_types = 1);

namespace App\Component\Product\Enum\Doctrine;

use App\Component\Product\Enum\ProductType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class ProductTypeType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return Type::TEXT;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ProductType
    {
        return null !== $value
            ? ProductType::byValue($value)
            : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var ProductType $value */
        return null !== $value
            ? (string) $value->getValue()
            : null;
    }

    public function getName(): string
    {
        return 'product_type';
    }
}
