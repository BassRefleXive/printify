<?php

declare(strict_types = 1);

namespace App\Component\Product\Enum;

use MabeEnum\Enum;

final class ProductType extends Enum
{
    public const T_SHORT = 't-short';
    public const CUP = 'cup';
}
