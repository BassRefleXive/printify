<?php

declare(strict_types = 1);


namespace App\Component\Product\Dto;

class ProductDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var float
     */
    public $price;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $size;

    /**
     * @var int
     */
    public $color;
}
