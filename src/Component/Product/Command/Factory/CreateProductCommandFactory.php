<?php

declare(strict_types = 1);

namespace App\Component\Product\Command\Factory;

use App\Component\Product\Command\CreateProductCommand;
use App\Component\Product\Enum\ProductSize;
use App\Component\Product\Enum\ProductType;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Money\Currency;
use Money\Money;

class CreateProductCommandFactory
{
    private $params;

    public function __construct(ParamFetcherInterface $params)
    {
        $this->params = $params;
    }

    public function create(): CreateProductCommand
    {
        // Any additional input parameters validation must be performed here.
        return new CreateProductCommand(
            new Money(
                $this->params->get('price'),
                new Currency($this->params->get('currency'))
            ),
            ProductType::byName($this->params->get('type')),
            ProductSize::byName($this->params->get('size')),
            (int) $this->params->get('color')
        );
    }
}
