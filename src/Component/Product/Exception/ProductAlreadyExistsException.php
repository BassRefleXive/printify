<?php

declare(strict_types = 1);

namespace App\Component\Product\Exception;

use App\Component\Product\Enum\ProductSize;
use App\Component\Product\Enum\ProductType;

class ProductAlreadyExistsException extends \LogicException implements ProductExceptionInterface
{
    final public static function notUnique(ProductType $type, ProductSize $size, int $color): self
    {
        return new self(
            sprintf(
                'Product of type "%s" with size "%s" and color "%d" already exists.',
                $type->getName(),
                $size->getName(),
                $color
            )
        );
    }
}
