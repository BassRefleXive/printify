<?php

declare(strict_types = 1);


namespace App\Component\Product\Exception;

use App\Component\Core\Exception\ApplicationExceptionInterface;

interface ProductExceptionInterface extends ApplicationExceptionInterface
{
}
