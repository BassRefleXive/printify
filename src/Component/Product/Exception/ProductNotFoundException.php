<?php

declare(strict_types = 1);


namespace App\Component\Product\Exception;

use Ramsey\Uuid\UuidInterface;

class ProductNotFoundException extends \RuntimeException implements ProductExceptionInterface
{
    final public static function missingById(UuidInterface $id): self
    {
        return new self(sprintf('Product with id "%s" does not exists.', $id));
    }
}
