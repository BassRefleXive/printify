<?php

declare(strict_types = 1);

namespace App\Component\Product\Assembler;

use App\Component\Product\Dto\ProductDto;
use App\Component\Product\Model\Product;

class ProductDtoAssembler
{
    public function fromModel(Product $product): ProductDto
    {
        $dto = new ProductDto();

        $dto->id = (string) $product->id();
        $dto->price = $product->price()->getAmount();
        $dto->currency = $product->price()->getCurrency()->getName();
        $dto->type = $product->type()->getName();
        $dto->size = $product->size()->getName();
        $dto->color = $product->color();

        return $dto;
    }
}
