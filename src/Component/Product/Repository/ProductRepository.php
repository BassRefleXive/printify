<?php

declare(strict_types = 1);

namespace App\Component\Product\Repository;

use App\Application\Repository\Product\ProductRepositoryInterface;
use App\Component\Core\Doctrine\Repository\BaseRepository;
use App\Component\Product\Enum\ProductSize;
use App\Component\Product\Enum\ProductType;
use App\Component\Product\Exception\ProductAlreadyExistsException;
use App\Component\Product\Exception\ProductNotFoundException;
use App\Component\Product\Model\Product;
use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function checkUniqueness(ProductType $type, ProductSize $size, int $color): void
    {
        $product = $this->findOneBy([
            'type'  => $type,
            'size'  => $size,
            'color' => $color,
        ]);

        if (null !== $product) {
            throw ProductAlreadyExistsException::notUnique($type, $size, $color);
        }
    }

    public function findAll(): array
    {
        return $this->findBy([]);
    }

    public function getById(UuidInterface $id): Product
    {
        /** @var Product $product */
        if (null === $product = $this->find($id)) {
            throw ProductNotFoundException::missingById($id);
        }

        return $product;
    }
}
