<?php

declare(strict_types = 1);

namespace App\Component\Order\Assembler;

use App\Component\Location\Assembler\CountryDtoAssembler;
use App\Component\Order\Dto\OrderDto;
use App\Component\Order\Model\Order;

class OrderDtoAssembler
{
    private $itemDtoAssembler;
    private $countryDtoAssembler;

    public function __construct()
    {
        $this->itemDtoAssembler = new ItemDtoAssembler();
        $this->countryDtoAssembler = new CountryDtoAssembler();
    }

    public function fromModel(Order $order): OrderDto
    {
        $dto = new OrderDto();

        $dto->id = (string) $order->id();
        $dto->ip = $order->ip();
        $dto->createdAt = $order->createdAt();
        $dto->updatedAt = $order->updatedAt();
        $dto->country = $this->countryDtoAssembler->fromModel($order->country());
        $dto->items = array_map([$this->itemDtoAssembler, 'fromModel'], $order->items());

        return $dto;
    }
}
