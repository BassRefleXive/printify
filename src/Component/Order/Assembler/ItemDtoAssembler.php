<?php

declare(strict_types = 1);

namespace App\Component\Order\Assembler;

use App\Component\Order\Dto\ItemDto;
use App\Component\Order\Model\Item;
use App\Component\Product\Assembler\ProductDtoAssembler;

class ItemDtoAssembler
{
    private $productDtoAssembler;

    public function __construct()
    {
        $this->productDtoAssembler = new ProductDtoAssembler();
    }

    public function fromModel(Item $item): ItemDto
    {
        $dto = new ItemDto();

        $dto->id = (string) $item->id();
        $dto->count = $item->count();
        $dto->product = $this->productDtoAssembler->fromModel($item->product());

        return $dto;
    }
}
