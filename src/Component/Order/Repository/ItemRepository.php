<?php

declare(strict_types = 1);

namespace App\Component\Order\Repository;

use App\Application\Repository\Order\ItemRepositoryInterface;
use App\Component\Core\Doctrine\Repository\BaseRepository;
use App\Component\Order\Model\Item;
use Doctrine\Common\Persistence\ManagerRegistry;

class ItemRepository extends BaseRepository implements ItemRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }
}
