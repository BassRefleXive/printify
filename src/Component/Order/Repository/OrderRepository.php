<?php

declare(strict_types = 1);

namespace App\Component\Order\Repository;

use App\Application\Repository\Order\OrderRepositoryInterface;
use App\Component\Core\Doctrine\Repository\BaseRepository;
use App\Component\Order\Command\FindOrderCommand;
use App\Component\Order\Exception\OrderNotFoundException;
use App\Component\Order\Model\Order;
use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function getOrdersCountByLocationInPeriod(string $countryIso2Code, int $period): int
    {
        $qb = $this->createQueryBuilder('o');

        $qb->select('COUNT(o)')
            ->innerJoin('o.country', 'c')
            ->where('c.codeIso2 = :codeIso2')
            ->andWhere('o.createdAt >= :period_start')
            ->setParameter('codeIso2', $countryIso2Code)
            ->setParameter('period_start', (new \DateTime())->sub(new \DateInterval(sprintf('PT%dS', $period)))->format('Y-m-d H:i:s'));

        return (int) $qb->getQuery()->getSingleScalarResult();
    }


    public function getById(UuidInterface $id): Order
    {
        /** @var Order $order */
        if (null === $order = $this->find($id)) {
            throw OrderNotFoundException::missingById($id);
        }

        return $order;
    }

    public function findByCommand(FindOrderCommand $command): array
    {
        $qb = $this->createQueryBuilder('o');

        $qb->select('o');

        if (null !== $command->productType()) {
            $qb->leftJoin('o.items', 'i')
                ->leftJoin('i.product', 'p')
                ->where('p.type = :product_type')
                ->setParameter('product_type', $command->productType()->getValue());
        }

        return $qb->getQuery()->getResult();
    }
}
