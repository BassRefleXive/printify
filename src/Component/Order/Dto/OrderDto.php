<?php

declare(strict_types = 1);

namespace App\Component\Order\Dto;

use App\Component\Location\Dto\CountryDto;

class OrderDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $ip;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var string
     */
    public $updatedAt;

    /**
     * @var CountryDto
     */
    public $country;

    /**
     * @var ItemDto[]
     */
    public $items;
}
