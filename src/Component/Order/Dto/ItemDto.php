<?php

declare(strict_types = 1);

namespace App\Component\Order\Dto;

use App\Component\Product\Dto\ProductDto;

class ItemDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var int
     */
    public $count;

    /**
     * @var ProductDto
     */
    public $product;
}
