<?php

declare(strict_types = 1);


namespace App\Component\Order\Service;

use App\Application\Repository\Order\OrderRepositoryInterface;
use App\Component\Location\Model\Country;

class LocationLimitCheckerService
{
    private $orderRepository;
    private $period;
    private $limit;

    public function __construct(OrderRepositoryInterface $orderRepository, int $period, int $limit)
    {
        $this->orderRepository = $orderRepository;
        $this->period = $period;
        $this->limit = $limit;
    }

    public function isLimitReached(Country $country): bool
    {
        return $this->orderRepository->getOrdersCountByLocationInPeriod($country->codeIso2(), $this->period) >= $this->limit;
    }
}
