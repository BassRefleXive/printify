<?php

declare(strict_types = 1);

namespace App\Component\Order\Service;

use App\Application\Repository\Order\ItemRepositoryInterface;
use App\Application\Repository\Order\OrderRepositoryInterface;
use App\Component\Location\Discoverer\Exception\DiscovererExceptionInterface;
use App\Component\Location\Discoverer\LocationDiscoverer;
use App\Component\Location\Service\LocationService;
use App\Component\Order\Assembler\OrderDtoAssembler;
use App\Component\Order\Command\CreateOrderCommand;
use App\Component\Order\Command\FindOrderCommand;
use App\Component\Order\Dto\OrderDto;
use App\Component\Order\Exception\InvalidOrderItemsException;
use App\Component\Order\Exception\LocationOrderLimitReachedException;
use App\Component\Order\Model\Item;
use App\Component\Order\Model\Order;
use Ramsey\Uuid\UuidInterface;

class OrderService
{
    private $orderRepository;
    private $itemRepository;
    private $locationService;
    private $locationDiscoverer;
    private $locationLimitCheckerService;
    private $orderDtoAssembler;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ItemRepositoryInterface $itemRepository,
        LocationService $locationService,
        LocationDiscoverer $locationDiscoverer,
        LocationLimitCheckerService $locationLimitCheckerService
    ) {
        $this->orderRepository = $orderRepository;
        $this->itemRepository = $itemRepository;
        $this->locationService = $locationService;
        $this->locationDiscoverer = $locationDiscoverer;
        $this->locationLimitCheckerService = $locationLimitCheckerService;
        $this->orderDtoAssembler = new OrderDtoAssembler();
    }

    public function create(CreateOrderCommand $createOrderCommand): OrderDto
    {
        try {
            $country = $this->locationDiscoverer->country($createOrderCommand->ip());
        } catch (DiscovererExceptionInterface $e) {
            $country = $this->locationService->defaultCountry();
        }

        if (count($createOrderCommand->createItemCommands()) < 1) {
            throw InvalidOrderItemsException::emptyOrder();
        }

        if ($this->locationLimitCheckerService->isLimitReached($country)) {
            throw LocationOrderLimitReachedException::create($country);
        }

        $order = new Order($this->orderRepository->nextIdentity(), $country, $createOrderCommand->ip());

        foreach ($createOrderCommand->createItemCommands() as $createItemCommand) {
            $order->addItem(
                new Item(
                    $this->itemRepository->nextIdentity(),
                    $createItemCommand->product(),
                    $order,
                    $createItemCommand->count()
                )
            );
        }

        if (($price = $order->price()) < 10) {
            throw InvalidOrderItemsException::priceTooSmall($price, 10.);
        }

        $this->orderRepository->save($order);

        return $this->orderDtoAssembler->fromModel($order);
    }

    public function price(UuidInterface $orderId): float
    {
        return $this->orderRepository->getById($orderId)->price();
    }

    public function find(FindOrderCommand $command): array
    {
        return array_map([$this->orderDtoAssembler, 'fromModel'], $this->orderRepository->findByCommand($command));
    }
}
