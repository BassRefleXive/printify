<?php

declare(strict_types = 1);


namespace App\Component\Order\Exception;

use Ramsey\Uuid\UuidInterface;

class OrderNotFoundException extends \RuntimeException implements OrderExceptionInterface
{
    final public static function missingById(UuidInterface $id): self
    {
        return new self(sprintf('Order with id "%s" does not exists.', $id));
    }
}
