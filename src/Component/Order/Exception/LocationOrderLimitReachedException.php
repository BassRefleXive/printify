<?php

declare(strict_types = 1);


namespace App\Component\Order\Exception;

use App\Component\Location\Model\Country;

class LocationOrderLimitReachedException extends \RuntimeException implements OrderExceptionInterface
{
    final public static function create(Country $country): self
    {
        return new self(sprintf('Orders limit for country "%s" reached.', $country->codeIso2()));
    }
}
