<?php

declare(strict_types = 1);


namespace App\Component\Order\Exception;

class InvalidOrderItemsException extends \LogicException implements OrderExceptionInterface
{
    final public static function emptyOrder(): self
    {
        return new self('Order should contain at least one product.');
    }

    final public static function priceTooSmall(float $actualPrice, float $minimalPrice): self
    {
        return new self(
            sprintf(
                'Minimal order price is "%.2f". Actual: "%.2f".',
                $minimalPrice,
                $actualPrice
            )
        );
    }
}
