<?php

declare(strict_types = 1);

namespace App\Component\Order\Exception;

use App\Component\Core\Exception\ApplicationExceptionInterface;

interface OrderExceptionInterface extends ApplicationExceptionInterface
{
}
