<?php

declare(strict_types = 1);

namespace App\Component\Order\Model;

use App\Component\Product\Model\Product;
use Ramsey\Uuid\UuidInterface;

class Item
{
    private $id;
    private $count;
    private $product;
    private $order;

    public function __construct(UuidInterface $id, Product $product, Order $order, int $count)
    {
        $this->id = $id;
        $this->product = $product;
        $this->order = $order;
        $this->count = $count;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function count(): int
    {
        return $this->count;
    }

    public function product(): Product
    {
        return $this->product;
    }

    public function order(): Order
    {
        return $this->order;
    }
}
