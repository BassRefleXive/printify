<?php

declare(strict_types = 1);

namespace App\Component\Order\Model;

use App\Component\Location\Model\Country;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

class Order
{
    private $id;
    private $ip;
    private $createdAt;
    private $updatedAt;
    private $country;
    private $items;

    public function __construct(UuidInterface $id, Country $country, string $ip)
    {
        $this->id = $id;
        $this->ip = $ip;
        $this->country = $country;
        $this->items = new ArrayCollection();
        $this->createdAt = $this->updatedAt = new \DateTime();
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function ip(): string
    {
        return $this->ip;
    }

    public function createdAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function updatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function incrementUpdatedAt(): self
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    public function country(): Country
    {
        return $this->country;
    }

    public function items(): array
    {
        return $this->items->toArray();
    }

    public function addItem(Item $item): self
    {
        $this->items->add($item);

        return $this;
    }

    public function price(): float
    {
        // Actually in this method we should check each product currency and convert it to the base currency, not just sum.
        // Currency converter should be passed in this method.
        return array_sum(
            array_map(
                function (Item $item): float {
                    return $item->product()->price()->getAmount() * $item->count();
                },
                $this->items()
            )
        );
    }
}
