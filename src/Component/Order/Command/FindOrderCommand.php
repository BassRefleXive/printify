<?php

declare(strict_types = 1);


namespace App\Component\Order\Command;

use App\Component\Product\Enum\ProductType;

class FindOrderCommand
{
    private $productType;

    public function __construct(?ProductType $productType)
    {
        $this->productType = $productType;
    }

    public function productType(): ?ProductType
    {
        return $this->productType;
    }
}
