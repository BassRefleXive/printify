<?php

declare(strict_types = 1);

namespace App\Component\Order\Command\Factory;

use App\Application\Repository\Product\ProductRepositoryInterface;
use App\Component\Order\Command\CreateItemCommand;
use Ramsey\Uuid\UuidInterface;

class CreateItemCommandFactory
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function create(UuidInterface $productId, int $count): CreateItemCommand
    {
        return new CreateItemCommand(
            $this->productRepository->getById($productId),
            $count
        );
    }
}
