<?php

declare(strict_types = 1);


namespace App\Component\Order\Command\Factory;

use App\Component\Order\Command\CreateItemCommand;
use App\Component\Order\Command\CreateOrderCommand;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Ramsey\Uuid\Uuid;

class CreateOrderCommandFactory
{
    private $createItemCommandFactory;

    public function __construct(CreateItemCommandFactory $createItemCommandFactory)
    {
        $this->createItemCommandFactory = $createItemCommandFactory;
    }

    public function create(ParamFetcherInterface $params, string $ip): CreateOrderCommand
    {
        return new CreateOrderCommand(
            array_map(
                function (array $data): CreateItemCommand {
                    return $this->createItemCommandFactory->create(
                        Uuid::fromString($data['id']),
                        (int) $data['count']
                    );
                },
                $params->get('products')
            ),
            $ip
        );
    }
}
