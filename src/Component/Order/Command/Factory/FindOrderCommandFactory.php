<?php

declare(strict_types = 1);


namespace App\Component\Order\Command\Factory;

use App\Component\Order\Command\FindOrderCommand;
use App\Component\Product\Enum\ProductType;
use FOS\RestBundle\Request\ParamFetcherInterface;

class FindOrderCommandFactory
{
    private $params;

    public function __construct(ParamFetcherInterface $params)
    {
        $this->params = $params;
    }

    public function create(): FindOrderCommand
    {
        return new FindOrderCommand($this->productType());
    }

    private function productType(): ?ProductType
    {
        return null !== ($productType = $this->params->get('product_type'))
            ? ProductType::byName($productType)
            : null;
    }
}
