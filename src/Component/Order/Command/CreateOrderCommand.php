<?php

declare(strict_types = 1);

namespace App\Component\Order\Command;

class CreateOrderCommand
{
    private $createItemCommands;
    private $ip;

    public function __construct(array $createItemCommands, string $ip)
    {
        $this->createItemCommands = $createItemCommands;
        $this->ip = $ip;
    }

    /**
     * @return CreateItemCommand[]
     */
    public function createItemCommands(): array
    {
        return $this->createItemCommands;
    }

    public function ip(): string
    {
        return $this->ip;
    }
}
