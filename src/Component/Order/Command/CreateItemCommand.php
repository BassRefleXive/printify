<?php

declare(strict_types = 1);

namespace App\Component\Order\Command;

use App\Component\Product\Model\Product;

class CreateItemCommand
{
    private $product;
    private $count;

    public function __construct(Product $product, int $count)
    {
        $this->product = $product;
        $this->count = $count;
    }

    public function product(): Product
    {
        return $this->product;
    }

    public function count(): int
    {
        return $this->count;
    }
}
