<?php

declare(strict_types = 1);

namespace App\Component\Core\Doctrine\Repository;

use App\Application\Repository\BaseRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class BaseRepository extends ServiceEntityRepository implements BaseRepositoryInterface
{
    final public function nextIdentity(): UuidInterface
    {
        return Uuid::uuid4();
    }

    public function save($entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }
}
