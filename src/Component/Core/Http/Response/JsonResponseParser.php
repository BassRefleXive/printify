<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Response;

use App\Component\Core\Http\Exception\ResponseException;
use Psr\Http\Message\ResponseInterface;

class JsonResponseParser
{
    public function responseArray(ResponseInterface $response): array
    {
        $data = $response->getBody()->getContents();

        if (null === $data = json_decode($data, true)) {
            throw ResponseException::invalidFormat('json');
        }

        return $data;
    }
}
