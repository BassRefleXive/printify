<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Factory;

use App\Component\Core\Http\Criteria\UriCriteriaInterface;
use GuzzleHttp\Psr7\Uri;

class UriFactory implements UriFactoryInterface
{
    protected $endpointUrl;

    public function __construct(string $endpointUrl)
    {
        $this->endpointUrl = $endpointUrl;
    }

    public function create(UriCriteriaInterface $uriCriteria): string
    {
        $uri = new Uri($this->endpointUrl);

        return (string) $uri
            ->withPath(implode('', array_merge(
                [$uri->getPath()],
                mb_substr($uriCriteria->path(), 0, 1) !== '/'
                    ? ['/']
                    : [],
                [$uriCriteria->path()]
            )))
            ->withQuery($uriCriteria->query());
    }
}
