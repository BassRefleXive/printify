<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Factory;

use App\Component\Core\Http\Criteria\UriCriteriaInterface;

interface UriFactoryInterface
{
    public function create(UriCriteriaInterface $uriCriteria): string;
}
