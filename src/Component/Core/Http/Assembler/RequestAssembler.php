<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Assembler;

use App\Component\Core\Http\Criteria\RequestCriteriaInterface;
use App\Component\Core\Http\Factory\UriFactoryInterface;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class RequestAssembler
{
    private $uriFactory;

    public function __construct(UriFactoryInterface $uriFactory)
    {
        $this->uriFactory = $uriFactory;
    }

    public function fromCriteria(RequestCriteriaInterface $criteria): RequestInterface
    {
        return new Request(
            $criteria->method(),
            $this->uriFactory->create($criteria->uriCriteria()),
            $criteria->headers(),
            $criteria->body()
        );
    }
}
