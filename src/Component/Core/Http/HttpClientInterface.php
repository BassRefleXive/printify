<?php

declare(strict_types=1);

namespace App\Component\Core\Http;

use App\Component\Core\Http\Exception\HttpClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @throws HttpClientException
     */
    public function send(RequestInterface $request, array $options = []): ResponseInterface;
}
