<?php

declare(strict_types=1);

namespace App\Component\Core\Http\RequestBody\Factory;

use App\Component\Core\Http\RequestBody\BodyParamsInterface;

class UrlencodedBodyFactory extends RequestBodyFactory
{
    public function create(BodyParamsInterface $params): string
    {
        return urldecode(http_build_query($params->params()));
    }
}
