<?php

declare(strict_types=1);

namespace App\Component\Core\Http\RequestBody\Factory;

use App\Component\Core\Http\RequestBody\BodyParamsInterface;
use GuzzleHttp\Psr7\MultipartStream;

abstract class RequestBodyFactory
{
    /**
     * @return MultipartStream|string
     */
    abstract public function create(BodyParamsInterface $params);
}
