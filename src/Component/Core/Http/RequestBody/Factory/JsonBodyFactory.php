<?php

declare(strict_types=1);

namespace App\Component\Core\Http\RequestBody\Factory;

use App\Component\Core\Http\RequestBody\BodyParamsInterface;

class JsonBodyFactory extends RequestBodyFactory
{
    public function create(BodyParamsInterface $params): string
    {
        return json_encode($params->params());
    }
}
