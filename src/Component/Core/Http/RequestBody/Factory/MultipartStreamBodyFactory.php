<?php

declare(strict_types=1);

namespace App\Component\Core\Http\RequestBody\Factory;

use App\Component\Core\Http\RequestBody\BodyParamsInterface;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MultipartStreamBodyFactory extends RequestBodyFactory
{
    public function create(BodyParamsInterface $params): MultipartStream
    {
        $params = $params->params();

        $data = [];

        foreach ($params as $name => $contents) {
            $tmp = [
                'name' => $name,
                'contents' => $contents,
            ];

            if ($contents instanceof UploadedFile) {
                $tmp['contents'] = fopen($contents->getPathname(), 'r');
                $tmp['filename'] = $contents->getClientOriginalName();
            }

            $data[] = $tmp;
        }

        return new MultipartStream($data);
    }
}
