<?php

declare(strict_types=1);

namespace App\Component\Core\Http\RequestBody;

interface BodyParamsInterface
{
    public function params(): array;
}
