<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Exception;

use App\Component\Core\Exception\ApplicationExceptionInterface;

class ResponseException extends \RuntimeException implements ApplicationExceptionInterface
{
    public function __construct($message = '', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function invalidFormat(string $format): self
    {
        return new self(sprintf('Invalid response format. Expected "%s".', $format));
    }
}
