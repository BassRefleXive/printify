<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Exception;

use App\Component\Core\Exception\ApplicationExceptionInterface;

class HttpClientException extends \RuntimeException implements ApplicationExceptionInterface
{
}
