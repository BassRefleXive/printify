<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Criteria;

use GuzzleHttp\Psr7\MultipartStream;

interface RequestCriteriaInterface
{
    public function method(): string;

    public function uriCriteria(): UriCriteriaInterface;

    public function headers(): array;

    /**
     * @return MultipartStream|string|null
     */
    public function body();
}
