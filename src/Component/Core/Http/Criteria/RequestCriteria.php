<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Criteria;

use GuzzleHttp\Psr7\MultipartStream;

class RequestCriteria implements RequestCriteriaInterface
{
    private $uriCriteria;
    private $method;
    protected $headers;
    private $body;

    public function __construct(
        UriCriteriaInterface $uriCriteria,
        string $method = 'GET',
        $body = null,
        array $headers = []
    ) {
        if (null !== $body && !is_string($body) && !($body instanceof MultipartStream)) {
            throw new \InvalidArgumentException('If request body is provided it must be string or MultipartStream.');
        }

        $this->uriCriteria = $uriCriteria;
        $this->method = $method;
        $this->body = $body;
        $this->headers = $headers;
    }

    public function method(): string
    {
        return $this->method;
    }

    public function uriCriteria(): UriCriteriaInterface
    {
        return $this->uriCriteria;
    }

    public function headers(): array
    {
        return $this->headers;
    }

    public function body()
    {
        return $this->body;
    }
}
