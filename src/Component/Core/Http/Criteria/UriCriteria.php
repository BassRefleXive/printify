<?php

declare(strict_types=1);

namespace App\Component\Core\Http\Criteria;

use App\Component\Core\Http\Exception\UriException;

class UriCriteria implements UriCriteriaInterface
{
    protected $path;
    protected $pathParams;
    protected $queryParams;

    public function __construct(string $path, array $pathParams = [], array $queryParams = [])
    {
        $this->path = $path;
        $this->pathParams = $pathParams;
        $this->queryParams = $queryParams;
    }

    public static function pathCriteria(string $path, array $queryParams = []): self
    {
        return new static($path, [], $queryParams);
    }

    public function path(): string
    {
        $path = $this->path;

        if (preg_match_all('/{(?<param>[^}]*)}/', $path, $matches)) {
            foreach ($matches['param'] as $param) {
                if (null === $paramValue = $this->param($param)) {
                    throw UriException::missingPathParameter($path, $param);
                }

                $path = str_replace(sprintf('{%s}', $param), $paramValue, $path);
            }
        }

        return $path;
    }

    public function param(string $param)
    {
        return $this->pathParams[$param] ?? null;
    }

    public function query(): string
    {
        return http_build_query($this->queryParams);
    }
}
