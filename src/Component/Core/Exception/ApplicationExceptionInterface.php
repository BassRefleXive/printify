<?php

declare(strict_types = 1);

namespace App\Component\Core\Exception;

interface ApplicationExceptionInterface extends \Throwable
{
}
