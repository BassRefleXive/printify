<?php

declare(strict_types = 1);

namespace App\Application\Controller;

use App\Component\Product\Command\Factory\CreateProductCommandFactory;
use App\Component\Product\Service\ProductService;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/product")
 */
class ProductController
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="price")
     * @FOSRest\RequestParam(name="currency")
     * @FOSRest\RequestParam(name="type")
     * @FOSRest\RequestParam(name="color")
     * @FOSRest\RequestParam(name="size")
     */
    public function createAction(ParamFetcherInterface $params): View
    {
        $command = (new CreateProductCommandFactory($params))->create();

        $dto = $this->service->create($command);

        return View::create($dto, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Get("/")
     */
    public function findAction(): View
    {
        return View::create($this->service->all());
    }
}
