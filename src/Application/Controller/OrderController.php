<?php

declare(strict_types = 1);


namespace App\Application\Controller;

use App\Component\Order\Command\Factory\CreateOrderCommandFactory;
use App\Component\Order\Command\Factory\FindOrderCommandFactory;
use App\Component\Order\Service\OrderService;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @FOSRest\Route("/order")
 */
class OrderController
{
    private const DEFAULT_IP = '127.0.0.1';

    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    /**
     * @FOSRest\Post("/")
     *
     * @FOSRest\RequestParam(name="products")
     */
    public function create(CreateOrderCommandFactory $commandFactory, Request $request, ParamFetcherInterface $params): View
    {
        $command = $commandFactory->create($params, $request->getClientIp() ?? self::DEFAULT_IP);

        $order = $this->service->create($command);

        return View::create($order, Response::HTTP_CREATED);
    }

    /**
     * @FOSRest\Get("/{id}/price")
     */
    public function price(string $id): View
    {
        return View::create(['price' => $this->service->price(Uuid::fromString($id))]);
    }

    /**
     * @FOSRest\Get("/")
     *
     * @FOSRest\QueryParam(name="product_type", default=null, nullable=true)
     */
    public function findAction(ParamFetcherInterface $params): View
    {
        $command = (new FindOrderCommandFactory($params))->create();

        return View::create($this->service->find($command));
    }
}
