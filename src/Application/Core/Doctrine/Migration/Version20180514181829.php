<?php declare(strict_types=1);

namespace App\Application\Core\Doctrine\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180514181829 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE product (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , type text NOT NULL --(DC2Type:product_type)
        , size text NOT NULL --(DC2Type:product_size)
        , color INTEGER NOT NULL, price DOUBLE PRECISION NOT NULL, currency CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX type_size_color_UNIQUE ON product (type, size, color)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE product');
    }
}
