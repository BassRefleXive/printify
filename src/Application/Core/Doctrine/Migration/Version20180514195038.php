<?php declare(strict_types=1);

namespace App\Application\Core\Doctrine\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180514195038 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE "order" (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , country_id VARCHAR(3) DEFAULT NULL, ip CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F5299398F92F3E70 ON "order" (country_id)');
        $this->addSql('CREATE TABLE order_product (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , product_id CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , order_id CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , count INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2530ADE64584665A ON order_product (product_id)');
        $this->addSql('CREATE INDEX IDX_2530ADE68D9F6D38 ON order_product (order_id)');
        $this->addSql('CREATE TABLE country (code_iso3 VARCHAR(3) NOT NULL, code_iso2 CLOB NOT NULL, name CLOB NOT NULL, name_official CLOB NOT NULL, latitude NUMERIC(2, 8) NOT NULL, longitude NUMERIC(2, 8) NOT NULL, PRIMARY KEY(code_iso3))');
        $this->addSql('CREATE INDEX code_iso_2_UNIQUE ON country (code_iso2)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE order_product');
        $this->addSql('DROP TABLE country');
    }
}
