<?php declare(strict_types=1);

namespace App\Application\Core\Doctrine\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180514214221 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_F5299398F92F3E70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order AS SELECT id, country_id, ip FROM "order"');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('CREATE TABLE "order" (id CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        , country_id VARCHAR(3) DEFAULT NULL COLLATE BINARY, ip CLOB NOT NULL COLLATE BINARY, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_F5299398F92F3E70 FOREIGN KEY (country_id) REFERENCES country (codeIso3) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO "order" (id, country_id, ip) SELECT id, country_id, ip FROM __temp__order');
        $this->addSql('DROP TABLE __temp__order');
        $this->addSql('CREATE INDEX IDX_F5299398F92F3E70 ON "order" (country_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_F5299398F92F3E70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order AS SELECT id, country_id, ip FROM "order"');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('CREATE TABLE "order" (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , country_id VARCHAR(3) DEFAULT NULL, ip CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO "order" (id, country_id, ip) SELECT id, country_id, ip FROM __temp__order');
        $this->addSql('DROP TABLE __temp__order');
        $this->addSql('CREATE INDEX IDX_F5299398F92F3E70 ON "order" (country_id)');
    }
}
