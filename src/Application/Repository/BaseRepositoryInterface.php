<?php

declare(strict_types = 1);


namespace App\Application\Repository;

use Ramsey\Uuid\UuidInterface;

interface BaseRepositoryInterface
{
    public function nextIdentity(): UuidInterface;

    public function save($entity): void;
}
