<?php

declare(strict_types = 1);

namespace App\Application\Repository\Order;

use App\Application\Repository\BaseRepositoryInterface;

interface ItemRepositoryInterface extends BaseRepositoryInterface
{
}
