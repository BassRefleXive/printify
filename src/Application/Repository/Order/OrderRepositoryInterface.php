<?php

declare(strict_types = 1);

namespace App\Application\Repository\Order;

use App\Application\Repository\BaseRepositoryInterface;
use App\Component\Order\Command\FindOrderCommand;
use App\Component\Order\Model\Order;
use Ramsey\Uuid\UuidInterface;

interface OrderRepositoryInterface extends BaseRepositoryInterface
{
    public function getOrdersCountByLocationInPeriod(string $countryIso2Code, int $period): int;

    public function getById(UuidInterface $id): Order;

    public function findByCommand(FindOrderCommand $command): array;
}
