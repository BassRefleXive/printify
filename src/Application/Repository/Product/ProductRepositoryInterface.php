<?php

declare(strict_types = 1);

namespace App\Application\Repository\Product;

use App\Component\Product\Enum\ProductSize;
use App\Component\Product\Enum\ProductType;
use App\Application\Repository\BaseRepositoryInterface;
use App\Component\Product\Model\Product;
use Ramsey\Uuid\UuidInterface;

interface ProductRepositoryInterface extends BaseRepositoryInterface
{
    public function checkUniqueness(ProductType $type, ProductSize $size, int $color): void;

    public function findAll(): array;

    public function getById(UuidInterface $id): Product;
}
