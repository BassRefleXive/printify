<?php

declare(strict_types = 1);

namespace App\Application\Repository\Location;

use App\Application\Repository\BaseRepositoryInterface;
use App\Component\Location\Model\Country;

interface CountryRepositoryInterface extends BaseRepositoryInterface
{
    public function getByIso2Code(string $iso2Code): Country;
}
