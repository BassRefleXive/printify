Printify Test Task
==================

Steps to run project:
```
git clone git@bitbucket.org:BassRefleXive/printify.git && \
    cd printify/docker && \
    scripts/build-and-start dev && \
    scripts/composer install && \
    chmod -R 777 ../data/
```

API accept both application/json and application/x-www-form-urlencoded request body content types.

Product
=======

Create:
---------------
POST http://127.0.0.1:8082/product/
Body Parameters:
```json
{
    "price":1.55,
    "currency":"USD",
    "type":"enum T_SHORT|CUP",
    "color":12345677,
    "size":"enum S|M|L|XL"
}
```

List all
--------------------
GET http://127.0.0.1:8082/product/


Order
=====

Create
------
POST http://127.0.0.1:8082/order/
```json
{
    "products": [
        {
            "id":"0eec88ca-7715-4644-a5fd-9e1985770f5b",
            "count": 10
        }
    ]
}
```

Price
-----
GET http://127.0.0.1:8082/order/0eec88ca-7715-4644-a5fd-9e1985770f5b/price

Find
----
GET http://127.0.0.1:8082/order/?product_type={enum T_SHORT|CUP}